from openpyxl import load_workbook

from test_date_selection import args
from test_matching_ids import total_transaction_value, pay_in_list, pay_out_list, utr_list, amount_list, cid_list, cname_list, created_at_list, unmatched_cust_to_onep_rzp_payin_list, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2
from test_matching_ids import unmatched_cust_to_onep_cf_payin_list, unmatched_onep_to_ret_payout_list, unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list, settlement_ids_list_sheeet_2, unmatched_cname_list, unmatched_cname_list_2


# Writing to excel sheet
def main():
    global workbook
    workbook = load_workbook("rzp_cf.xlsx")

    writing_matched_info()
    writing_unmatched_pin_info()
    writing_unmatched_pout_info()
    writing_summary()

    workbook.save("rzp_cf.xlsx")

def writing_matched_info():
    global workbook
    matched_info_sheet = workbook.create_sheet(f"{args.start_month}{args.start_day}")
    matched_info_sheet.append(["pay_in", "pay_out", "utr", "amount (in Rupees)", "cid", "cname", "created_at", "settlement_id", "settlement_amount"])

    for i in range(len(pay_in_list)):
        matched_info_sheet.append([pay_in_list[i], pay_out_list[i], utr_list[i], amount_list[i], cid_list[i], cname_list[i], created_at_list[i], settlement_ids_list_sheeet_2[i]])

def writing_unmatched_pin_info():
    global workbook
    unmatched_info_sheet_payin = workbook.create_sheet(f"{args.start_month}{args.start_day}(unmatched-payin)")
    unmatched_info_sheet_payin.append(["pay_in", "amount (in Rupees)", "cid", "cname", "created_at"])

    for i in range(len(unmatched_cust_to_onep_rzp_payin_list)):
        unmatched_info_sheet_payin.append([unmatched_cust_to_onep_rzp_payin_list[i], unmatched_amount_list_2[i], unmatched_cid_list_2[i], unmatched_cname_list_2[i], unmatched_created_at_list_2[i]])

    for i in range(len(unmatched_cust_to_onep_cf_payin_list)):
        unmatched_info_sheet_payin.append([unmatched_cust_to_onep_cf_payin_list[i], "", "", "", ""])

def writing_unmatched_pout_info():
    global workbook
    unmatched_info_sheet_payout = workbook.create_sheet(f"{args.start_month}{args.start_day}(unmatched-payout)")
    unmatched_info_sheet_payout.append(["pay_out", "amount (in Rupees)", "cid", "cname", "created_at"])

    for i in range(len(unmatched_onep_to_ret_payout_list)):
        unmatched_info_sheet_payout.append([unmatched_onep_to_ret_payout_list[i], unmatched_amount_list[i], unmatched_cid_list[i], unmatched_cname_list[i], unmatched_created_at_list[i]])

def writing_summary():
    global workbook
    summary_sheet = workbook["summary"]
    summary_sheet.append([f"{args.start_month}{args.start_day}", total_transaction_value])

if __name__ == "__main__":
    main()

