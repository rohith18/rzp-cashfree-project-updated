import argparse


def main():
    global args, from_date, to_date, base_time_stamp
    args, from_date, to_date = parsing_args()

def parsing_args():
    global base_time_stamp
    parser = argparse.ArgumentParser()

    parser.add_argument("--start-month", type=int, required=True)
    parser.add_argument("--start-day", type=int, required=True)
    parser.add_argument("--num-days", type=int, required=True)

    args = parser.parse_args()

    base_time_stamp = get_base_time_stamp(args.start_month)

    from_date = base_time_stamp + (args.start_day - 1)*86400
    to_date = from_date + args.num_days*(86400)

    return args, from_date, to_date

def get_base_time_stamp(start_month):
    global base_time_stamp

    # January 1st
    if start_month == 1:
        base_time_stamp = 1640995200
    # Febrauary 1st
    if start_month == 2:
        base_time_stamp = 1643673600
    # March 1st
    if start_month == 3:
        base_time_stamp = 1646092800
    # April 1st
    if start_month == 4:
        base_time_stamp = 1648771200

    return base_time_stamp

main()