import copy

from test_fetching_json_data import cust_to_onep_rzp_list, cust_to_onep_cf_list, onep_to_ret_rzp_list
from utils import unix_timestamp_to_date, removing_duplicates, extract_company_name, settlement_api

final_settlement_ids_list, not_settlement_type_list, cust_to_onep_rzp_payin_list_ids, cust_to_onep_rzp_list_2, onep_to_ret_rzp_payout_list_ids, onep_to_ret_rzp_list_2 = [[] for _ in range(6)]
matching_count, total_transaction_value = 0, 0
utr_list, pay_out_list, pay_in_list, amount_list, cid_list, created_at_list, unmatched_cust_to_onep_cf_payin_list = [[] for _ in range(7)]
settlement_ids_list_sheet_list, final_settlement_ids_list_sheet_list, settlement_amount_sheet_2, settlement_ids_list_sheeet_2, settlement_ids_list_sheet, cname_list = [[] for _ in range(6)]
unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2 = [[] for i in range(6)]
settlement_amount_list_sheeet_2, unmatched_cname_list, unmatched_cname_list_2 = [[] for _ in range(3)]

def main():
    global final_settlement_ids_list, not_settlement_type_list, cust_to_onep_rzp_payin_list_ids, cust_to_onep_rzp_list_2, onep_to_ret_rzp_payout_list_ids
    global matching_count, total_transaction_value, unmatched_cust_to_onep_cf_list, onep_to_ret_rzp_list_2, cname_list
    global utr_list, pay_out_list, pay_in_list, amount_list, cid_list, created_at_list, unmatched_cust_to_onep_cf_payin_list 
    global settlement_ids_list_sheet_list, final_settlement_ids_list_sheet_list, settlement_amount_sheet_2, settlement_ids_list_sheeet_2, settlement_ids_list_sheet
    global unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2
    global settlement_amount_list_sheeet_2, unmatched_cname_list, unmatched_cname_list_2, unmatched_cust_to_onep_rzp_payin_list, unmatched_onep_to_ret_payout_list

    getting_settlement_ids()
    matching_cf_rzpout()
    unmatched_cust_to_onep_cf_list, unmatched_cust_to_onep_cf_payin_list = getting_unmatched_cf_ids(unmatched_cust_to_onep_cf_list, unmatched_cust_to_onep_cf_payin_list)
    adding_valid_ids_rzpin()
    adding_valid_ids_rzpout()
    cust_to_onep_rzp_payin_list_ids, onep_to_ret_rzp_payout_list_ids = removing_duplicates_from_pin_pout(cust_to_onep_rzp_payin_list_ids, onep_to_ret_rzp_payout_list_ids)
    matching_rzpin_rzpout()

    cname_list = extract_company_name(cid_list)
    extracting_unmatched_pout_info()
    extracting_unmatched_rzpin_rzpout()
    extracting_unmatched_cfpin_rzpout()
    unmatched_cname_list, unmatched_cname_list_2 = extracting_unmatched_cname_list(unmatched_cname_list, unmatched_cname_list_2)

# Getting settlement ids
def getting_settlement_ids():
    global onep_to_ret_rzp_list, settlement_ids_list, final_settlement_ids_list
    for i in onep_to_ret_rzp_list:
        try:
            if i.get("source").get("notes").get("type") == "settlement":
                settlement_ids_list = settlement_api(i.get("source").get("notes").get("id"))
                for item in settlement_ids_list:
                    final_settlement_ids_list.append(item)
        except AttributeError:
            pass

# Matching cashfree reference ids with pay_out {onep_to_ret_rzp_list}
def matching_cf_rzpout():
    global final_settlement_ids_list, cust_to_onep_cf_list, settlement_ids_list_sheeet_2, settlement_amount_list_sheeet_2, matching_count, unmatched_cust_to_onep_cf_list
    global pay_in_list, pay_out_list, utr_list, amount_list, total_transaction_value, created_at_list, cid_list, unmatched_cust_to_onep_cf_list
    unmatched_cust_to_onep_cf_list = copy.deepcopy(cust_to_onep_cf_list)

    for i in final_settlement_ids_list:
        for j in cust_to_onep_cf_list:
            try:
                if i.get("id") == str(j.get("referenceId")):
                    settlement_ids_list_sheeet_2.append(i.get("settlementId"))
                    settlement_amount_list_sheeet_2.append(i.get("amount"))
                    pay_in_list.append(i.get("id"))
                    pay_out_list.append(j.get("referenceId"))
                    utr_list.append(j.get("utr"))
                    amount_list.append(float(j.get("amount")))
                    total_transaction_value += float(j.get("amount"))
                    created_at_list.append(j.get("paymentTime"))
                    cid_list.append("")

                    unmatched_cust_to_onep_cf_list.remove(j)
                    matching_count += 1
            except AttributeError:
                pass
       
# Getting umnatched cashfree ids
def getting_unmatched_cf_ids(unmatched_cust_to_onep_cf_list, unmatched_cust_to_onep_cf_payin_list):
    for i in unmatched_cust_to_onep_cf_list:
        unmatched_cust_to_onep_cf_payin_list.append(str(i.get("referenceId")))

    return unmatched_cust_to_onep_cf_list, unmatched_cust_to_onep_cf_payin_list

# Adding only valid ids to cust_to_onep_rzp_list
def adding_valid_ids_rzpin():
    global cust_to_onep_rzp_list, cust_to_onep_rzp_payin_list_ids, cust_to_onep_rzp_list_2
    for item in cust_to_onep_rzp_list:
        try:
            if item.get("id"):
                cust_to_onep_rzp_payin_list_ids.append(item.get("id"))
                cust_to_onep_rzp_list_2.append(item)
        except AttributeError:
            pass

# Adding only valid ids to onep_to_ret_rzp_list
def adding_valid_ids_rzpout():
    global onep_to_ret_rzp_list, onep_to_ret_rzp_payout_list_ids, onep_to_ret_rzp_list_2
    for item in onep_to_ret_rzp_list:
        try:
            if (item.get("source").get("notes").get("id") and item.get("source").get("status") == "processed"):
                onep_to_ret_rzp_payout_list_ids.append(item.get("source").get("notes").get("id"))
                onep_to_ret_rzp_list_2.append(item)
        except AttributeError:
            pass  

# Removing duplicates from payin and payout ids
def removing_duplicates_from_pin_pout(cust_to_onep_rzp_payin_list_ids, onep_to_ret_rzp_payout_list_ids):
    global unmatched_cust_to_onep_rzp_payin_list, unmatched_onep_to_ret_payout_list
    cust_to_onep_rzp_payin_list_ids = removing_duplicates(cust_to_onep_rzp_payin_list_ids)
    onep_to_ret_rzp_payout_list_ids = removing_duplicates(onep_to_ret_rzp_payout_list_ids)

    unmatched_cust_to_onep_rzp_payin_list = copy.deepcopy(cust_to_onep_rzp_payin_list_ids)
    unmatched_onep_to_ret_payout_list = copy.deepcopy(onep_to_ret_rzp_payout_list_ids)

    return cust_to_onep_rzp_payin_list_ids, onep_to_ret_rzp_payout_list_ids

# Matching razorpay pay_in ids with pay_out {onep_to_ret_rzp_list}
def matching_rzpin_rzpout():
    global cust_to_onep_rzp_payin_list_ids, onep_to_ret_rzp_list, settlement_ids_list_sheeet_2, settlement_amount_list_sheeet_2, unmatched_onep_to_ret_payout_list
    global pay_in_list, pay_out_list, utr_list, amount_list, total_transaction_value, cid_list, created_at_list, unmatched_cust_to_onep_rzp_payin_list, matching_count
    for item_01 in cust_to_onep_rzp_payin_list_ids:
        for item in onep_to_ret_rzp_list:
            try:
                if item.get("source").get("notes").get("id") == item_01:
                    settlement_ids_list_sheeet_2.append("")
                    settlement_amount_list_sheeet_2.append("")
                    pay_in_list.append(item_01)
                    pay_out_list.append(item_01)
                    utr_list.append(item.get("source").get("utr"))
                    amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                    total_transaction_value += item.get("amount")
                    cid_list.append(item.get("source").get("notes").get("cid"))
                    created_at_list.append(item.get("created_at"))

                    unmatched_cust_to_onep_rzp_payin_list.remove(item_01)
                    unmatched_onep_to_ret_payout_list.remove(item_01)
                    matching_count += 1
            except AttributeError:
                pass
            except ValueError:
                pass

# Extracting the pout unmatched_ids information
def extracting_unmatched_pout_info():
    global unmatched_onep_to_ret_payout_list, onep_to_ret_rzp_list_2, unmatched_amount_list, unmatched_cid_list, unmatched_created_at_list
    for item_01 in unmatched_onep_to_ret_payout_list:
        for item in onep_to_ret_rzp_list_2:
            if item_01 == item.get("source").get("notes").get("id"):
                try:
                    unmatched_amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                    unmatched_cid_list.append(item.get("source").get("notes").get("cid"))
                    unmatched_created_at_list.append(item.get("created_at"))
                except AttributeError:
                    pass

# Extracting the razor p_in unmatched_ids information
def extracting_unmatched_rzpin_rzpout():
    global unmatched_cust_to_onep_rzp_payin_list, cust_to_onep_rzp_list_2, unmatched_amount_list_2, unmatched_cid_list_2, unmatched_created_at_list_2 
    for item_01 in unmatched_cust_to_onep_rzp_payin_list:
        for item in cust_to_onep_rzp_list_2:
            if item_01 == item.get("id"):
                try:
                    unmatched_amount_list_2.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                    unmatched_cid_list_2.append(item.get("notes").get("cid"))
                    unmatched_created_at_list_2.append(item.get("created_at"))
                except AttributeError:
                    pass

# Extracting the cashfree p_in unmatched_ids information
def extracting_unmatched_cfpin_rzpout():
    global unmatched_cust_to_onep_cf_payin_list, cust_to_onep_cf_list, unmatched_amount_list_2, unmatched_created_at_list_2
    for item_01 in unmatched_cust_to_onep_cf_payin_list:
        for item in cust_to_onep_cf_list:
            try:
                if item_01 == item.get("referenceId"):
                    unmatched_amount_list_2.append(item.get("amount"))
                    unmatched_created_at_list_2.append(item.get("paymentTime"))
            except AttributeError:
                    pass

# Extracting corresponding unmatched company name lists
def extracting_unmatched_cname_list(unmatched_cname_list, unmatched_cname_list_2):
    unmatched_cname_list = extract_company_name(unmatched_cid_list)
    unmatched_cname_list_2 = extract_company_name(unmatched_cid_list_2)

    return unmatched_cname_list, unmatched_cname_list_2

main()
